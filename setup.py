import setuptools

commitInfo = "$Format:%d$".strip("( )").split()
version = commitInfo[commitInfo.index("tag:") + 1].rstrip(",")

setuptools.setup(
    name="lamassemble",
    version=version,
    description='Merge overlapping "long" DNA reads into a consensus sequence',
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mcfrith/lamassemble",
    classifiers=[
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
        "License :: OSI Approved :: MIT License",
    ],
    scripts=[
        "lamassemble",
    ],
)
