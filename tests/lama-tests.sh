#! /bin/sh

cd $(dirname $0)

PATH=..:$PATH

mat=rel3-4-train.mat
fa=group25.fa

{
    lamassemble --help
    lamassemble $mat $fa
    awk 'BEGIN {print} 1' $fa | lamassemble --all $mat -
    lamassemble --all -a -p0.01 -W39 -m4 -z10 $mat $fa
    lamassemble --all -g20 -nMyName $mat $fa
    head -n1 $fa | lamassemble $mat -
    head -n569 $fa | lamassemble $mat -
    lamassemble -z10 -d100 $mat $fa
    lamassemble --end -z20 $mat $fa
    lamassemble -s2 -z20 $mat $fa
    lamassemble -c $mat group25-aln.fa
    lamassemble -c -fFASTQ $mat group25-aln.fa
    lamassemble PromethION-2019 $fa
} 2>&1 | diff -u lama-tests.out -
